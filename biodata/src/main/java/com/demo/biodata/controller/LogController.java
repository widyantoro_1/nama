package com.demo.biodata.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.biodata.assembler.LogAssembler;
import com.demo.biodata.dto.Logdto;
import com.demo.biodata.ent.Log;
import com.demo.biodata.repository.LogRepository;
import com.demo.biodata.service.LogService;

@RestController
@RequestMapping("/log")
@CrossOrigin(origins = "http://localhost:3000")
public class LogController {
	@Autowired
	LogRepository logrepo;
	
	@Autowired
	LogAssembler assem;
	
	@Autowired
	LogService serv;
	
	
	public Logdto convertToDto(Log ent) {
		Logdto dto = new Logdto();
		dto.setIdrequest(ent.getIdrequest());
		dto.setType(ent.getType());
		dto.setIdplatform(ent.getIdplat());
		dto.setPayment(ent.getPayment());
		dto.setNama(ent.getNama());
		return dto;
	}
	@GetMapping("/")
	public List<Logdto> get(){
		List<Log> ent = logrepo.findAll();
		List<Logdto> dto = ent.stream().map(biodata -> assem.fromEntity(biodata) ).collect(Collectors.toList());
		return dto;
	}
//	public Biodata convertToEntity(Biodto dto) {
//		Biodata ent = new Biodata();
//		ent.setAlamat(dto.getAlamat());
//		ent.setNama(dto.getNama());
//		return ent;
//	}
	@PostMapping("/")
	public void insert(@RequestBody Logdto dto) {
		
		serv.insert(dto);
	}
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable String id) {
		logrepo.deleteById(id);
	}
	@PutMapping("/edit/{id}")
	public void edit(@RequestBody Logdto newdto,@PathVariable String id) {
		if (logrepo.findById(id).isPresent()) {
			Logdto newbio = convertToDto(logrepo.findById(id).get());
			newbio.setIdrequest(id);
			newbio.setIdplatform(newdto.getIdplatform());
			newbio.setType(newdto.getType());
			newbio.setPayment(newdto.getPayment());
			newbio.setNama(newdto.getNama());
			serv.insert(newbio);
		}
	}

}
