package com.demo.biodata.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Logdto {
	private String idrequest;
	private String idplatform;
	private String nama;
	private String type;
	private String payment;
}
