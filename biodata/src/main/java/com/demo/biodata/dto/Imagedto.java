package com.demo.biodata.dto;

import com.demo.biodata.dto.Logdto.LogdtoBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Imagedto {
	private String idrequest;
	private Byte description;
}
