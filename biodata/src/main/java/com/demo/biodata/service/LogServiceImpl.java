package com.demo.biodata.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.biodata.assembler.LogAssembler;
import com.demo.biodata.dto.Logdto;
import com.demo.biodata.ent.Log;
import com.demo.biodata.repository.LogRepository;


@Service
@Transactional
public class LogServiceImpl implements LogService{
	@Autowired
	LogRepository logrepo;
	
	@Autowired
	LogAssembler assembler;
	

	@Override
	public Logdto insert(Logdto dto) {
		Log ent = logrepo.save(assembler.fromDto(dto));
		logrepo.save(ent);
		return assembler.fromEntity(ent);
		
		
	}

}
