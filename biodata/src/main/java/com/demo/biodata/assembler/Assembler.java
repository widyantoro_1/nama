package com.demo.biodata.assembler;

public interface Assembler <Entity,Dto>{
	Entity fromDto (Dto dto);
	Dto fromEntity (Entity ent);

}
