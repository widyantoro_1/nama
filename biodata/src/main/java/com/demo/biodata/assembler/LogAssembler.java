package com.demo.biodata.assembler;

import org.springframework.stereotype.Component;

import com.demo.biodata.dto.Logdto;
import com.demo.biodata.ent.Log;

@Component
public class LogAssembler implements Assembler<Log, Logdto>{

	@Override
	public Log fromDto(Logdto dto) {
		if (dto == null) {
		return null;
		}
		Log ent = new Log();
		if (dto.getIdrequest() != null) {
			ent.setIdrequest(dto.getIdrequest());
		}
		if (dto.getIdplatform()!=null) {
			ent.setIdplat(dto.getIdplatform());
		}
		if (dto.getType()!=null) {
			ent.setType(dto.getType());
		}
		if (dto.getPayment()!=null) {
			ent.setPayment(dto.getPayment());
		}
		if (dto.getNama()!=null) {
			ent.setNama(dto.getNama());
		}
		return ent;
	}

	@Override
	public Logdto fromEntity(Log ent) {
		if (ent == null) {
			return null;
		}
		return Logdto.builder()
				.idrequest(ent.getIdrequest())
				.idplatform(ent.getIdplat())
				.payment(ent.getPayment())
				.nama(ent.getNama())
				.type(ent.getType())
				.build();
			
		
	}

}
