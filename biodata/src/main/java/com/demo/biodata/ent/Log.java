package com.demo.biodata.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="log_widyantoro")
public class Log {
	@Id
	@Column(name="idrequestbooking", length = 255)
	private String idrequest;
	@Column(name= "id_platform", length = 20)
	private String idplat;
	@Column(name= "nama_platform", length = 20)
	private String nama;
	@Column(name="doc_type", length = 20)
	private String type;
	@Column(name="term_of_payment", length = 5)
	private String payment;

}
