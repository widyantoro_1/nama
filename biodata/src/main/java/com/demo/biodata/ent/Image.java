package com.demo.biodata.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="image_widyantoro")
public class Image {
	@Id
	@Column(name="idrequestbooking", length = 255)
	private String idrequest;
	@Column(name= "description")
	private Byte description;

}
