package com.demo.biodata.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.biodata.ent.Log;

public interface LogRepository extends JpaRepository<Log, String>{

}
