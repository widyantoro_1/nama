package com.demo.biodata.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.biodata.ent.Image;

public interface ImageRepository extends JpaRepository<Image, String>{

}
